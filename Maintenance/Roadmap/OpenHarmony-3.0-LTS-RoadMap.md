#### 一、维护职责

SIG Release会对LTS分支进行长期的支持和维护，维护内容主要为以下几点：

- 安全漏洞修复：包含OpenHarmony社区独有的漏洞，以及开源和三方软件发布的漏洞。漏洞通过[公告](https://gitee.com/openharmony/security/blob/master/zh/security-disclosure/README.md)发布。
- 缺陷修复：对社区反馈的缺陷issue进行修复，并在标签版本的[releasenotes](https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes)中对外公告。
- 特性变更：如果标签版本有相对应的修改，会在标签版本的[releasenotes](https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes)中公告。

#### 二、标签版本计划

1.Openharmony 3.0 LTS分支版本计划

| 版本计划               | 版本号                | 版本构建日期 | 版本转测试 | 版本测试完成 |
| ---------------------- | --------------------- | ------------ | ---------- | ------------ |
| Openharmony-v3.0-LTS   | Openharmony 3.0 LTS   | 2021/9/24    | 2021/9/24  | 2021/9/30    |
| Openharmony-v3.0.1-LTS | Openharmony 3.0.1 LTS | 2021/12/29   | 2021/12/29 | 2022/1/10    |
| Openharmony-v3.0.2-LTS | Openharmony 3.0.2 LTS | 2022/2/28    | 2022/2/28  | 2022/3/15    |
| Openharmony-v3.0.3-LTS | Openharmony 3.0.3 LTS | 2022/3/22    | 2022/3/22  | 2022/4/6     |
| Openharmony-v3.0.5-LTS | Openharmony 3.0.5 LTS | 2022/6/17    | 2022/6/17  | 2022/6/30    |
| Openharmony-v3.0.6-LTS | Openharmony 3.0.6 LTS | 2022/8/15    | 2022/8/15  | 2022/8/30    |

